package com.andrewsdevelopment.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ResourceBundle;

@Component
public class SleepHQBundle {

    private final ResourceBundle resourceBundle;

@Autowired
    public SleepHQBundle(ResourceBundle resourceBundle) {

        this.resourceBundle = resourceBundle;
    }

    public String getValue(String key) {
        return resourceBundle.getString(key);
    }

    public int getIntValue(String key) {
        return Integer.parseInt(getValue(key));
    }


}
