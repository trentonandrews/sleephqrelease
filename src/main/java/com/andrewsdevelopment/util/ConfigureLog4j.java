package com.andrewsdevelopment.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

public class ConfigureLog4j {

    public static void config(String applicationName, String logFileLocation) {
        try {
            ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
            builder.setConfigurationName(applicationName);

            AppenderComponentBuilder appenderBuilder;

            ComponentBuilder triggeringPolicy = builder.newComponent("Policies")
                    .addComponent(builder.newComponent("SizeBasedTriggeringPolicy").addAttribute("size", "20M"));

            appenderBuilder = builder.newAppender(applicationName, "RollingFile").addAttribute("fileName", logFileLocation + "SleepHQ.log")
                    .addComponent(triggeringPolicy).addAttribute("filePattern", logFileLocation + "SleepHQ_%d{dd-MM-yyyy HH-mm-ss}.log");


            LayoutComponentBuilder layout = builder.newLayout("PatternLayout").addAttribute("pattern", "%d [%t] %-5p %c - %m%n");

            appenderBuilder.add(layout);
            builder.add(appenderBuilder);

            RootLoggerComponentBuilder root = builder.newRootLogger(Level.DEBUG).add(builder.newAppenderRef(applicationName));

            builder.add(root);

            Configuration newLogConfig = builder.build();

  /*
    we know that the documented way to initalize the config is Configurator.initalize(config) but for some reason that does not work
    when there are existing loggers defined. calling start on the context seems to do the trick.

    if https://issues.apache.org/jira/browse/LOG4J2-1794 ever gets fixed maybe we can change our approach
   */
            LoggerContext.getContext(false).start(newLogConfig);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


}
