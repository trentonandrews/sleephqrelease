package com.andrewsdevelopment.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Properties;


@Component
public class EmailComplete {


    private final String smtpHost;
    private final int port;
    private final String emailAddress;
    private final String smtpUsername;
    private final String smtpPassword;

    private final String deployedLocation;

    @Autowired
    public EmailComplete(SleepHQBundle bundle) {
        smtpHost = bundle.getValue("smtpHost");
        port = bundle.getIntValue("port");
        emailAddress = bundle.getValue("emailAddress");
        smtpUsername = bundle.getValue("smtpUsername");
        smtpPassword = bundle.getValue("smtpPassword");
        deployedLocation = bundle.getValue("deployedLocation");
    }


    JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(smtpHost);
        mailSender.setPort(port);
        mailSender.setUsername(smtpUsername);
        mailSender.setPassword(smtpPassword);
        mailSender.setSession(getSession());
        return mailSender;
    }


    private MimeMessage createMineMessage() {

        Session session = getSession();

        return new MimeMessage(session);
    }

    private Session getSession() {
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", smtpHost);
        properties.put("mail.smtp.port", String.valueOf(port));
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(smtpUsername, smtpPassword);

            }

        });

        // Used to debug SMTP issues
        session.setDebug(true);
        return session;
    }


    public void send(String message) {
        try {
            JavaMailSender javaMailSender = javaMailSender();

            MimeMessage mm = javaMailSender.createMimeMessage();
            MimeMessageHelper msg = new MimeMessageHelper(mm, true);
            msg.setFrom(emailAddress);
            msg.setTo(emailAddress);
            msg.setSubject(message);
            msg.setText(message, false);

            File f = new File(deployedLocation + "SCRIPT.LOG");
            if (f.exists()) {
                msg.addAttachment("ScriptLog.log", f);
            }
            f = new File(deployedLocation + "SleepHQ.log");
            if (f.exists()) {
                msg.addAttachment("JavaLog.log", f);
            }
            javaMailSender.send(mm);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
