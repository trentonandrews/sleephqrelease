package com.andrewsdevelopment.util.zip;

import java.io.File;

public interface CPAPZipper {

    boolean zipDirectory(File dir, String zipDirName);
}
