package com.andrewsdevelopment.util.zip.impl;

import com.andrewsdevelopment.util.zip.CPAPZipper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@Component
public class ZipFilesAll implements CPAPZipper {

    Logger logger = LogManager.getLogger(this.getClass());

    private List<String> populateFilesList(List<String> filesListInDir, File dir, List<String> includeFiles) throws IOException {

        File[] files = dir.listFiles();
        for(File file : files){
            if (includeFiles == null || includeFiles.contains(file.getName())) {
                if (file.isFile()) filesListInDir.add(file.getAbsolutePath());
                else populateFilesList(filesListInDir, file, null);
            } else {
                logger.debug("Skipping " + file.getAbsoluteFile());
            }
        }
        return filesListInDir;
    }

    public boolean zipDirectory(File dir, String zipDirName) {
        logger.debug("Creating zip File " + zipDirName);
        boolean hasFiles = false;
        try {
            List<String> filesListInDir = new ArrayList<>();
            populateFilesList(filesListInDir, dir, List.of("STR.edf", "SETTINGS", "DATALOG", "Identification.tgt", "Identification.crc"));
            hasFiles = filesListInDir.size() > 0;
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String filePath : filesListInDir){
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
            logger.debug("Done with file. " + filesListInDir.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hasFiles;
    }

}
