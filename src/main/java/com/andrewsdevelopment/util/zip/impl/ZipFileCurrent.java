package com.andrewsdevelopment.util.zip.impl;

import com.andrewsdevelopment.util.zip.CPAPZipper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFileCurrent implements CPAPZipper {

    Logger logger = LogManager.getLogger(this.getClass());

    private List<String> populateFilesList(List<String> filesListInDir, File dir) throws IOException {

        if (!dir.exists()) {
            return filesListInDir;
        }
        File[] files = dir.listFiles();
        for(File file : files){
                if (file.isFile()) filesListInDir.add(file.getAbsolutePath());
                else populateFilesList(filesListInDir, file);
        }
        return filesListInDir;
    }

    public static void main(String[] args) {
        ZipFileCurrent z = new ZipFileCurrent();
        z.zipDirectory(new File("/Users/andrews/CPAP"), "/Users/andrews/Desktop/CPAP2.zip");
    }

    public boolean zipDirectory(File dir, String zipDirName) {
        logger.debug("Creating zip File " + zipDirName);
        boolean hasFiles = false;
        try {
            List<String> filesListInDir = new ArrayList<>();

            filesListInDir.add((new File(dir, "STR.edf")).getAbsolutePath());
            filesListInDir.add((new File(dir, "Identification.crc")).getAbsolutePath());
            filesListInDir.add((new File(dir, "Identification.tgt")).getAbsolutePath());
            populateFilesList(filesListInDir, new File(dir, "SETTINGS"));
            populateFilesList(filesListInDir, new File(dir,"DATALOG/" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
            populateFilesList(filesListInDir, new File(dir,"DATALOG/" + LocalDate.now().minusDays(1l).format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
            populateFilesList(filesListInDir, new File(dir,"DATALOG/" + LocalDate.now().minusDays(2l).format(DateTimeFormatter.ofPattern("yyyyMMdd"))));

            hasFiles = filesListInDir.size() > 0;
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String filePath : filesListInDir){
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
            logger.debug("Done with file. " + filesListInDir.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hasFiles;
    }
}
