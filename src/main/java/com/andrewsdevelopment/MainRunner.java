package com.andrewsdevelopment;

import com.andrewsdevelopment.configuration.SleepHQConfiguration;
import com.andrewsdevelopment.util.ConfigureLog4j;
import com.andrewsdevelopment.util.SleepHQBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainRunner {

    public static void main(String[] args) throws Exception{

        ApplicationContext ac = new AnnotationConfigApplicationContext(SleepHQConfiguration.class);
        SleepHQBundle bundle = ac.getBean(SleepHQBundle.class);
        ConfigureLog4j.config("SleepHQ", bundle.getValue("deployedLocation"));
        Logger logger = LogManager.getLogger(UploadSleepDataToSleepHQ.class);

        logger.debug("Starting");
        UploadSleepDataToSleepHQ uploadSleepDataToSleepHQ = ac.getBean(UploadSleepDataToSleepHQ.class);
        uploadSleepDataToSleepHQ.execute();
        logger.debug("Complete");
    }



}
