package com.andrewsdevelopment.website;

import com.andrewsdevelopment.util.Constant;
import com.andrewsdevelopment.util.SleepHQBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.Duration;

@Component
public class SleepHQWebSite {

    Logger logger = LogManager.getLogger(this.getClass());

    private WebDriver driver;
    private final SleepHQBundle sleepHQBundle;

    @Autowired
    public SleepHQWebSite(SleepHQBundle sleepHQBundle) {
        this.sleepHQBundle = sleepHQBundle;
        System.setProperty("webdriver.chrome.driver", this.sleepHQBundle.getValue("chromeDriverLocation"));
    }

    public void init() {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        driver = new ChromeDriver();

    }



    private void loginToSleepHQ() {
        driver.get(sleepHQBundle.getValue("loginURL"));
        driver.findElement(By.id(sleepHQBundle.getValue("usernameID"))).sendKeys(sleepHQBundle.getValue("username"));
        driver.findElement(By.id(sleepHQBundle.getValue("passwordID"))).sendKeys(sleepHQBundle.getValue("password"));
        driver.findElement(By.xpath(sleepHQBundle.getValue("loginButtonXPath"))).click();
    }

    private void logout() {
        driver.get(sleepHQBundle.getValue("logoutURL"));
        driver.close();
    }


    private void dropAndUploadFile() throws InterruptedException {
        driver.get(sleepHQBundle.getValue("uploadURL"));
        WebElement droparea = driver.findElement(By.id(sleepHQBundle.getValue("uploadFilesID")));
        dropFile(new File(sleepHQBundle.getValue("transmitZipName")), droparea, 0, 0);
        driver.findElement(By.id(sleepHQBundle.getValue("uploadButtonID"))).click();
        Thread.sleep(Duration.ofSeconds(120L));
    }

    public void execute() throws InterruptedException {
        init();
        logger.debug("Logging into website");
        loginToSleepHQ();
        logger.debug("Uploading data");
       dropAndUploadFile();
        logger.debug("logout");
        logout();
    }

    private void dropFile(File filePath, WebElement target, int offsetX, int offsetY) {
        if (!filePath.exists()) {
            throw new WebDriverException("File not found: " + filePath.toString());
        }

        WebDriver driver = ((RemoteWebElement) target).getWrappedDriver();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30L));
        doFile(jse, target, filePath, offsetX, offsetY, wait);
    }

    private void doFile(JavascriptExecutor jse, WebElement target, File filePath, int offsetX, int offsetY,  WebDriverWait wait) {
        WebElement input = (WebElement) jse.executeScript(Constant.JS_DROP_FILE, target, offsetX, offsetY);
        input.sendKeys(filePath.getAbsoluteFile().toString());
        wait.until(ExpectedConditions.stalenessOf(input));

    }


}
