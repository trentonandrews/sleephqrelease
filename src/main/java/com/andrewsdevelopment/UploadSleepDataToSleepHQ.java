package com.andrewsdevelopment;


import com.andrewsdevelopment.util.EmailComplete;
import com.andrewsdevelopment.util.SleepHQBundle;
import com.andrewsdevelopment.util.zip.CPAPZipper;
import com.andrewsdevelopment.website.SleepHQWebSite;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
@Component
public class UploadSleepDataToSleepHQ {


    private final SleepHQBundle sleepHQBundle;
    private final SleepHQWebSite sleepHQWebSite;

    private final CPAPZipper zip;

    private final EmailComplete emailComplete;

    private Logger logger = LogManager.getLogger(UploadSleepDataToSleepHQ.class);;

    @Autowired
    public UploadSleepDataToSleepHQ(SleepHQBundle sleepHQBundle, SleepHQWebSite sleepHQWebSite, CPAPZipper zip, EmailComplete emailComplete) {
        this.sleepHQBundle = sleepHQBundle;
        this.sleepHQWebSite = sleepHQWebSite;
        this.zip = zip;
        this.emailComplete = emailComplete;
    }



    public void execute() throws Exception {
        deleteFiles();
        boolean hasFile = false;
        zipDirectory();
        logger.debug("Zipping File");
        hasFile = true;
        copyFile();
        backFile();

        if (hasFile) {
           sleepHQWebSite.execute();
           deleteFiles();
        }

        sendEmail();
        deleteLogs();;
    }

    private void deleteFiles() {
        logger.debug("Deleting file");
        (new File(sleepHQBundle.getValue("zipName"))).delete();
        (new File(sleepHQBundle.getValue("transmitZipName"))).delete();
    }

    private void sendEmail() {
        emailComplete.send("Sleep HQ Upload Complete.");
    }
    private void deleteLogs() {
        logger.debug("Deleting file");
        (new File(sleepHQBundle.getValue("deployedLocation") + "SCRIPT.LOG")).delete();
        (new File(sleepHQBundle.getValue("deployedLocation") + "SleepHQ.LOG")).delete();
    }


    private void backFile() throws IOException {
        logger.debug("Backup File Action");
        String backupFile = sleepHQBundle.getValue("backupPathName") + "CPAP_" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + ".zip";
        copyFile(new File(sleepHQBundle.getValue("zipName")), new File(backupFile));
    }

    private void copyFile() throws IOException {
        logger.debug("Copy File Action");
        copyFile(new File(sleepHQBundle.getValue("zipName")), new File(sleepHQBundle.getValue("transmitZipName")));
    }

    private void copyFile(File input, File output) throws IOException {
        logger.debug("     Copy file " + input.getAbsolutePath() + " to " + output.getAbsolutePath());
        FileInputStream fis = new FileInputStream(input);
        FileOutputStream fos = new FileOutputStream(output);
        IOUtils.copy(fis, fos);
        fos.close();
        fis.close();
    }

    private boolean zipDirectory() {
        return zip.zipDirectory(new File(sleepHQBundle.getValue("pathName")), sleepHQBundle.getValue("zipName"));
    }




}