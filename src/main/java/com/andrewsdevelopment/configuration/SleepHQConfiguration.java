package com.andrewsdevelopment.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.ResourceBundle;

@Configuration
@ComponentScan("com.andrewsdevelopment")
public class SleepHQConfiguration {

    @Bean
    public ResourceBundle resourceBundle() {
        return ResourceBundle.getBundle("sleepHQ");
    }



}
